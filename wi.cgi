#!/usr/bin/perl
use strict;
use warnings;

# absolute fs-path which will be prepended to the project path
#our $projectroot = "/pub/scm";
our $projectroot = "/git/pub";

# target of the home link on top of all pages
our $home_link = '/gate/git/wi/';

# string of the home link on top of all pages
our $home_link_str = 'melon git Repositories';

# name of your site or organization to appear in page titles
# replace this with something more descriptive for clearer bookmarks
our $site_name = 'melon git Repositories';

# URI of stylesheets
our @stylesheets = ('/gate/git/gitweb');

# URI of GIT logo (72x27 size)
our $logo = q</admin/logo/suikalogo>;
# URI of GIT favicon, assumed to be image/png type
our $favicon = q</favicon>;

# URI and label (title) of GIT logo link
our $logo_url = q</admin/cvs/>;
our $logo_label = 'melon git Repositories - suika.fam.cx';
our $logo_alt = 'Suika';

# list of git base URLs used for URL to where fetch project from,
# i.e. full URL is "$git_base_url/$project"
our @git_base_url_list = ('git@melon:/git/pub');

require 'gitweb.pl';

1;

__END__

=head1 NAME

wi.cgi - melon git Repositories Web Interface CGI Script

=head1 AUTHOR

Wakaba <w@suika.fam.cx>.

=head1 LICENSE

Copyright 2010 Wakaba <w@suika.fam.cx>.  All rights reserved.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.

=cut
